package der.promotivator.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.greenrobot.eventbus.EventBus;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import der.promotivator.Constants;
import der.promotivator.StateMachine;
import der.promotivator.model.Message;
import der.promotivator.receiver.event.NewCurrentMessageEvent;
import der.promotivator.service.NotificationService;
import der.promotivator.util.MessagesUtil;
import der.promotivator.util.Settings;

import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;
import static der.promotivator.PromotivatorApp.statemachine;

/**
 * Created by simon on 08.02.16.
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {

    private static final long PERIOD = 1000L * 60L * 60L * 24L;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            if (statemachine.get() == StateMachine.AppState.Ready) {

                if(settings.getBoolean(Settings.Key.PostMessagesEnabled, true)) {
                    Intent startNotification = new Intent(context, NotificationService.class);
                    startNotification.setAction(Constants.Action.StartNotification.toString());
                    startWakefulService(context, startNotification);
                }

                long lastUpdate = settings.getLong(Settings.Key.LastUpdate, 0L);
                if (lastUpdate + PERIOD < System.currentTimeMillis()) {
                    Message random = MessagesUtil.nextRandom();
                    if (random != null) {
                        settings.save(Settings.Key.CurrentMessage, objectMapper.writeValueAsString(random));
                        settings.save(Settings.Key.LastUpdate, System.currentTimeMillis());
                        EventBus.getDefault().post(new NewCurrentMessageEvent());
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    public static void schedule(Context context) {

        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                AlarmManager.INTERVAL_HOUR, alarmIntent);
    }
}
