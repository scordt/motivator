package der.promotivator.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;

import android.support.v7.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;


import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.sql.SQLException;

import der.promotivator.Constants;
import der.promotivator.R;
import der.promotivator.model.Message;
import der.promotivator.receiver.AlarmReceiver;
import der.promotivator.service.event.OnDismissClickedEvent;
import der.promotivator.ui.MainActivity;
import der.promotivator.util.Settings;

import static der.promotivator.PromotivatorApp.database;
import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;

/**
 * Created by simon on 07.02.16.
 */
public class NotificationService extends Service {


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            switch (Constants.Action.fromIntent(intent)) {

                case StartNotification:
                    postNotification(intent);
                    AlarmReceiver.completeWakefulIntent(intent);
                    break;
                case StopNotification:
                    closeNotification(intent);
                    break;
            }
            //postNotification(intent);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }

    private void closeNotification(Intent intent) {
        EventBus.getDefault().post(new OnDismissClickedEvent());
        stopForeground(true);
        stopSelf();
    }

    private void postNotification(Intent intent) throws SQLException, IOException {

        Message currentMessage = objectMapper.readValue(settings.getString(Settings.Key.CurrentMessage, ""), Message.class);

        if (currentMessage != null) {
            Notification notification = buildNotification(currentMessage);
            startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE,
                    notification);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private Notification buildNotification(Message message) {

        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction(Constants.Action.NotificationAction.toString());
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pNotificationIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);


        Intent closeIntent = new Intent(this, NotificationService.class);
        closeIntent.setAction(Constants.Action.StopNotification.toString());
        PendingIntent pCloseIntent = PendingIntent.getService(this, 0,
                closeIntent, 0);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        Bitmap bitmap = null;
        if( message.getImage() != null && message.getImage().length > 0) {
            bitmap = BitmapFactory.decodeByteArray(message.getImage(), 0, message.getImage().length, options);
        }

        Builder notification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getAuthor())
                .setTicker(message.getMessage())
                .setContentText(message.getMessage())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pNotificationIntent)
                .setOngoing(true)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.stop_posting_messages),
                        pCloseIntent);
        if(bitmap != null) {
            notification.setLargeIcon(
                    Bitmap.createScaledBitmap(bitmap, 128, 128, false));
        }
        return notification.build();
    }

}
