package der.promotivator.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import der.promotivator.Constants;
import der.promotivator.StateMachine;
import der.promotivator.model.Message;
import der.promotivator.model.OAuth;
import der.promotivator.service.event.MessagesLoadingCompleteEvent;
import der.promotivator.util.EncodingUtil;
import der.promotivator.util.Http;
import der.promotivator.util.Settings;
import der.promotivator.util.MessagesUtil;

import static der.promotivator.PromotivatorApp.database;
import static der.promotivator.PromotivatorApp.http;
import static der.promotivator.PromotivatorApp.network;
import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;
import static der.promotivator.PromotivatorApp.statemachine;
import static java.lang.Thread.sleep;

/**
 * Created by simon on 07.02.16.
 */
public class DownloadService extends IntentService {

    public static final Map<String, String> authBody = new HashMap<>();

    static {
        authBody.put("username", EncodingUtil.encodeURIComponent(Constants.CREDENTIALS.USERNAME));
        authBody.put("password", EncodingUtil.encodeURIComponent(Constants.CREDENTIALS.PASSWORD));
        authBody.put("grant_type", "password");
        authBody.put("scope", "read%20write");
        authBody.put("client_id", Constants.CREDENTIALS.CLIENT_ID);
        authBody.put("client_secret", Constants.CREDENTIALS.CLIENT_SECRET);
    }

    public DownloadService() {
        super(DownloadService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (network.isConnectedToWiFi())
            downloadAndPersist();

    }

    private void downloadAndPersist() {

        try {
            long backoff = 0L;

            // retry to auth and download for 3 times,
            // with an exponential backoff,
            // starting with 1.5 seconds
            for (int i = 0; i < 3; i++) {

                backoff = (long) (1500L * Math.pow(2, i));
                sleep(backoff);

                if (!isAuthenticated())
                    authenticate();
                try {

                    String newHash = http.get(Constants.URL.GET_HASH);
                    String oldHash = settings.getString(Settings.Key.Hash, "");

                    if (!newHash.equals(oldHash)) {
                        InputStream in = null;
                        try {
                            statemachine.set(StateMachine.AppState.Loading);
                            // since we stream the shit out of the request,
                            // we can safly fetch 5k objects at once
                            in = http.getInputStream(Constants.URL.GET_MESSAGES + "?size=5000");
                            JsonParser parser = new JsonFactory().createParser(in);
                            parser.nextToken();
                            while (parser.nextToken() == JsonToken.START_OBJECT) {
                                Message msg = objectMapper.readValue(parser, Message.class);
                                database.getMessageDao().create(msg);
                            }
                        } finally {
                            if (in != null) {
                                in.close();
                                settings.save(Settings.Key.Hash, newHash);
                                if (settings.getString(Settings.Key.CurrentMessage, null) == null) {
                                    Message random = MessagesUtil.nextRandom();
                                    if(random != null) {
                                        settings.save(Settings.Key.CurrentMessage, objectMapper.writeValueAsString(random));
                                        settings.save(Settings.Key.LastUpdate, System.currentTimeMillis());
                                    }
                                }
                                statemachine.set(StateMachine.AppState.Ready);
                            }
                        }
                    }
                } catch (Http.NotAuthorizedException e) {
                    // reset oauth stuff, since auth failed
                    settings.save(Settings.Key.OAuth, "");
                    e.printStackTrace();
                }
            }
        } catch (IOException | InterruptedException | SQLException | Http.ClientErrorException e) {
            e.printStackTrace();
        }
    }

    private boolean isAuthenticated() {
        try {
            OAuth oauth = objectMapper.readValue(settings.getString(Settings.Key.OAuth, ""), OAuth.class);
            return oauth != null && oauth.getExpires_at() >= System.currentTimeMillis();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void authenticate() {
        try {
            String response = http.postFormData(Constants.URL.POST_AUTH, authBody);
            OAuth oauth = OAuth.fromJson(response);
            oauth.setExpires_at(System.currentTimeMillis() + (oauth.getExpires_in() * 1000));
            oauth.setClient_id(Constants.CREDENTIALS.CLIENT_ID);
            oauth.setClient_secret(Constants.CREDENTIALS.CLIENT_SECRET);
            settings.save(Settings.Key.OAuth, oauth.toJson());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Http.ClientErrorException e) {
            e.printStackTrace();
        } catch (Http.NotAuthorizedException e) {
            e.printStackTrace();
        }
    }
}
