package der.promotivator;

import android.content.Intent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 05.02.16.
 */
public class Constants {

    public interface URL {

        String BASE_URL     = "http://softrare.eu:9090/";
        String POST_AUTH    = BASE_URL + "oauth/token";
        String GET_HASH     = BASE_URL + "api/messages/hash";
        String GET_MESSAGES = BASE_URL + "api/messages";
    }

    public interface CREDENTIALS {
        String USERNAME       = ";)";
        String PASSWORD       = ";)";
        String CLIENT_ID = "motivatorserverapp";
        String CLIENT_SECRET  = "mySecretOAuthSecret";
    }

    public interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 1;
    }

    public enum Action {

        NotificationAction("der.promotivator.notification"),
        StartNotification("der.promotivator.start.foreground"),
        StopNotification("der.promotivator.stop.foreground"),
        MessageIdExtra("der.promotivator.message"),
        StartBackgroundAction("der.promotivator.start.background"),
        StopBackgroundAction("der.promotivator.stop.background");

        private static final Map<String, Action> valuesById;
        static {
            valuesById = new HashMap<String, Action>();
            for (final Action state : Action.values()) {
                valuesById.put(state.id, state);
            }
        }

        private final String id;

        Action(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return id;
        }

        public static Action fromIntent(final Intent intent) {
            return valuesById.get(intent.getAction());
        }

    }
}
