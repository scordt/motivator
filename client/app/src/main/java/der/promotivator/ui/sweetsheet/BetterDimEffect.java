package der.promotivator.ui.sweetsheet;

import android.graphics.Color;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mingle.sweetpick.Effect;

/**
 * Created by simon on 05.02.16.
 */
public class BetterDimEffect implements Effect {

    private float Value;

    public BetterDimEffect(float value) {
        Value = value;
    }

    public void setValue(float value) {
        Value = value;
    }

    @Override
    public float getValue() {
        return Value;
    }

    @Override
    public void effect(ViewGroup vp, ImageView view) {
        view.setBackgroundColor(Color.argb((int) (250 * Value), 0, 0, 0));
    }
}
