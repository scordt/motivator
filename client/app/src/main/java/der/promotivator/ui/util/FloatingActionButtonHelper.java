package der.promotivator.ui.util;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Created by simon on 05.02.16.
 */
public class FloatingActionButtonHelper {

    private FloatingActionButton fab;

    public FloatingActionButtonHelper(AppCompatActivity activity, int resId) {
        fab = (FloatingActionButton) activity.findViewById(resId);
    }

    public FloatingActionButtonHelper setOnClickListener(View.OnClickListener listener) {
        fab.setOnClickListener(listener);
        return this;
    }

    public FloatingActionButton build() {
        return fab;
    }

    public FloatingActionButtonHelper hide() {
        fab.hide();
        return this;
    }

    public FloatingActionButtonHelper show() {
        fab.show();
        return this;
    }
}
