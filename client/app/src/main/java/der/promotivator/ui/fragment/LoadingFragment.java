package der.promotivator.ui.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import der.promotivator.R;
import der.promotivator.ui.event.OnActivityResultEvent;
import der.promotivator.ui.event.OnConnectedEvent;
import der.promotivator.ui.event.OnDisconnectedEvent;

import static der.promotivator.PromotivatorApp.network;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoadingFragment extends Fragment {


    private TextView textView;

    public LoadingFragment() {
    }

    public static LoadingFragment newInstance() {
        return new LoadingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loading, container, false);;
        textView = (TextView) view.findViewById(R.id.textView);

        if(network.isConnectedToWiFi())
            textView.setText(getString(R.string.loading_progress));
        else
            textView.setText(getString(R.string.no_wifi_title));

        return view;
    }

    @Subscribe()
    public void onEvent(OnActivityResultEvent e) {
        textView.setText(getString(R.string.waiting_for_network_update));
    }

    @Subscribe()
    public void onEvent(OnConnectedEvent e) {
        textView.setText(getString(R.string.loading_progress));
    }

    @Subscribe()
    public void onEvent(OnDisconnectedEvent e) {
        textView.setText(getString(R.string.no_wifi_title));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
