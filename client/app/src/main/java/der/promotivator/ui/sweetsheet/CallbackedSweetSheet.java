package der.promotivator.ui.sweetsheet;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.mingle.sweetpick.SweetSheet;

/**
 * Created by simon on 05.02.16.
 */
public class CallbackedSweetSheet extends SweetSheet {

    private OnSweetSheetAnimationListener listener;

    public interface OnSweetSheetAnimationListener {
        void onSweetSheetShow();
        void onSweetSheetHide();
    }

    public CallbackedSweetSheet(RelativeLayout parentVG) {
        super(parentVG);
    }

    public CallbackedSweetSheet(FrameLayout parentVG) {
        super(parentVG);
    }

    public CallbackedSweetSheet(ViewGroup parentVG) {
        super(parentVG);
    }

    @Override
    public void show() {
        if(!isShow()) {
            super.show();
            if (listener != null)
                listener.onSweetSheetShow();
        }
    }

    @Override
    public void dismiss() {
        if(isShow()) {
            super.dismiss();
            if (listener != null)
                listener.onSweetSheetHide();
        }
    }


    public void setOnSweetSheetAnimationListener(OnSweetSheetAnimationListener listener) {
        this.listener = listener;
    }
}
