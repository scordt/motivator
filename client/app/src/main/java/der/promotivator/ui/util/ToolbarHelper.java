package der.promotivator.ui.util;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import der.promotivator.R;


/**
 * Created by simon on 04.02.16.
 */
public class ToolbarHelper {

    private final AppCompatActivity activity;
    private final Toolbar toolbar;

    public ToolbarHelper(AppCompatActivity activity, int resId) {
        this.activity = activity;
        this.toolbar = (Toolbar) activity.findViewById(resId);
        activity.setSupportActionBar(toolbar);
        //toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setTitle(activity.getString(R.string.app_name));
    }

    public ToolbarHelper hide() {
        activity.getSupportActionBar().hide();
        return this;
    }

    public ToolbarHelper show() {
        activity.getSupportActionBar().show();
        return this;
    }

    /*public ToolbarHelper animateIn() {
        toolbar.setVisibility(View.VISIBLE);
        toolbar.animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator())
                .start();
        return this;
    }

    public ToolbarHelper animateOut() {
        toolbar.setVisibility(View.VISIBLE);
        toolbar.animate()
                .translationY(-toolbar.getBottom())
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        toolbar.setVisibility(View.INVISIBLE);
                    }
                })
                .start();
        return this;
    }*/

    public Toolbar build() {
        return toolbar;
    }
}
