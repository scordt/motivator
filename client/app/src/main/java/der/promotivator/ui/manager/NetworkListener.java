package der.promotivator.ui.manager;

/**
 * Created by simon on 04.02.16.
 */
public interface NetworkListener {

    void onConnectedViaWifi();

    void onDisconnected();
}
