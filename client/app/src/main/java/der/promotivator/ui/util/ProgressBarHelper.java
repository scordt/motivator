package der.promotivator.ui.util;

import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import der.promotivator.util.Settings;

import static der.promotivator.PromotivatorApp.settings;

/**
 * Created by simon on 05.02.16.
 */
public class ProgressBarHelper {

    private CircularProgressView progressBar;

    public ProgressBarHelper(AppCompatActivity activity, int resId) {
        progressBar = (CircularProgressView) activity.findViewById(resId);
    }

    public ProgressBarHelper setColor(int color) {
        progressBar.setColor(color);
        return this;
    }

    public CircularProgressView build() {
        return progressBar;
    }

    public ProgressBarHelper hide() {
        progressBar.setVisibility(View.GONE);
        return this;
    }

    public ProgressBarHelper show() {
        if(settings.getInt(Settings.Key.NumMessages, 0) == 0)
            progressBar.setVisibility(View.VISIBLE);
        return this;
    }
}
