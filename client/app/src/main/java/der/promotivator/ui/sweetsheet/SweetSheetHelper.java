package der.promotivator.ui.sweetsheet;

import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mingle.sweetpick.CustomDelegate;

import der.promotivator.R;


/**
 * Created by simon on 05.02.16.
 */
public class SweetSheetHelper {

    private final AppCompatActivity activity;
    private final CallbackedSweetSheet sweetSheet;
    private TextView message;
    private TextView author;

    public SweetSheetHelper(AppCompatActivity activity, int resId) {
        this.activity = activity;
        sweetSheet = new CallbackedSweetSheet((RelativeLayout) activity.findViewById(resId));
    }

    public SweetSheetHelper init(int resId) {
        CustomDelegate customDelegate = new CustomDelegate(true,
                CustomDelegate.AnimationType.DuangAnimation);
        View view = LayoutInflater.from(activity).inflate(resId, null, false);
        customDelegate.setCustomView(view);
        sweetSheet.setDelegate(customDelegate);
        message = (TextView) view.findViewById(R.id.message);
        author = (TextView) view.findViewById(R.id.author);
        return this;
    }

    public CallbackedSweetSheet build() {
        return sweetSheet;
    }

    public SweetSheetHelper enableDimEffect() {
        sweetSheet.setBackgroundEffect(new BetterDimEffect(16));
        return this;
    }

    public SweetSheetHelper setAnimationListener(CallbackedSweetSheet.OnSweetSheetAnimationListener listener) {
        sweetSheet.setOnSweetSheetAnimationListener(listener);
        return this;
    }

    public SweetSheetHelper setBackgroundClickEnable(boolean enable) {
        sweetSheet.setBackgroundClickEnable(enable);
        return this;
    }

    public SweetSheetHelper show() {
        if (!sweetSheet.isShow())
            sweetSheet.show();
        return this;
    }

    public SweetSheetHelper hide() {
        if (sweetSheet.isShow())
            sweetSheet.dismiss();
        return this;
    }

    public boolean isShown() {
        return sweetSheet.isShow();
    }

    public SweetSheetHelper setMessage(String message) {
        this.message.setText(message);
        return this;
    }

    public SweetSheetHelper setAuthor(String author) {
        this.author.setText(author);
        return this;
    }
}
