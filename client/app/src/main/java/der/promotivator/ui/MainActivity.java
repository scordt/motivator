package der.promotivator.ui;


import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;

import der.promotivator.Constants;
import der.promotivator.R;
import der.promotivator.StateMachine;
import der.promotivator.model.Message;
import der.promotivator.receiver.event.NewCurrentMessageEvent;
import der.promotivator.service.NotificationService;
import der.promotivator.service.event.OnDismissClickedEvent;
import der.promotivator.ui.event.OnActivityResultEvent;
import der.promotivator.ui.event.OnConnectedEvent;
import der.promotivator.ui.event.OnDisconnectedEvent;
import der.promotivator.ui.fragment.ImageFragment;
import der.promotivator.ui.fragment.LoadingFragment;
import der.promotivator.ui.manager.NetworkListener;
import der.promotivator.ui.manager.NetworkManager;
import der.promotivator.ui.sweetsheet.CallbackedSweetSheet;
import der.promotivator.ui.sweetsheet.SweetSheetHelper;
import der.promotivator.ui.util.FloatingActionButtonHelper;
import der.promotivator.ui.util.SnackbarHelper;
import der.promotivator.ui.util.ToolbarHelper;
import der.promotivator.util.Settings;

import static der.promotivator.PromotivatorApp.network;
import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;
import static der.promotivator.PromotivatorApp.statemachine;

public class MainActivity extends AppCompatActivity implements
        StateMachine.OnStateChangeListener,
        View.OnClickListener,
        CallbackedSweetSheet.OnSweetSheetAnimationListener,
        NetworkListener {

    private SweetSheetHelper sweetSheetHelper;
    private FloatingActionButtonHelper actionButtonHelper;
    private ToolbarHelper toolbarHelper;
    private NetworkManager networkManager;
    private SnackbarHelper snackbarHelper;
    private Message currentMessage;
    private Menu optionsMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);

        actionButtonHelper = new FloatingActionButtonHelper(this, R.id.fab)
                .setOnClickListener(this)
                .hide();

        sweetSheetHelper = new SweetSheetHelper(this, R.id.root)
                .init(R.layout.content_sweetsheet)
                .enableDimEffect()
                .setAnimationListener(this)
                .setBackgroundClickEnable(false)
                .hide();

        toolbarHelper = new ToolbarHelper(this, R.id.toolbar).hide();

        snackbarHelper = new SnackbarHelper(this, R.string.no_wifi);
        snackbarHelper
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    }
                })
                .hide();

        statemachine.register(this);

        networkManager = new NetworkManager(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fab)
            switch (statemachine.get()) {
                case Ready:
                    if (sweetSheetHelper.isShown())
                        sweetSheetHelper.hide();
                    else sweetSheetHelper.show();

            }
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkManager.unregisterReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        networkManager.registerReceiver();
        switch (statemachine.get()) {

            case Init:
            case Loading:
                showFragment(LoadingFragment.newInstance());
                toolbarHelper.hide();
                actionButtonHelper.hide();
                sweetSheetHelper.hide();
                break;
            case Ready:
                showFragment(ImageFragment.newInstance());
                toolbarHelper.show();
                actionButtonHelper.show();
                sweetSheetHelper.show();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        statemachine.unregister(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {

            EventBus.getDefault().post(new OnActivityResultEvent());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!network.isConnectedToWiFi())
                        networkManager.triggerCallbacks();
                }
            }, 8000L);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    void showFragment(Fragment fragment) {
        getSupportFragmentManager().popBackStack();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void onStateChange(StateMachine.AppState prevState, StateMachine.AppState nextState) {
        Handler mainHandler = new Handler(getMainLooper());

        Runnable onMainThreadRunnable = new Runnable() {
            @Override
            public void run() {
                switch (statemachine.get()) {
                    case Init:
                    case Loading:
                        toolbarHelper.hide();
                        actionButtonHelper.hide();
                        sweetSheetHelper.hide();
                        showFragment(LoadingFragment.newInstance());
                        return;
                    default:
                        toolbarHelper.show();
                        actionButtonHelper.show();
                        sweetSheetHelper.show();
                        showFragment(ImageFragment.newInstance());
                }
            }
        };

        mainHandler.post(onMainThreadRunnable);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        optionsMenu = menu;

        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem it = menu.findItem(R.id.stop_posting);
        if (settings.getBoolean(Settings.Key.PostMessagesEnabled, true)) {
            it.setTitle(getString(R.string.stop_posting_messages));
        } else {
            it.setTitle(getString(R.string.get_new_message));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.stop_posting:
                if (settings.getBoolean(Settings.Key.PostMessagesEnabled, true)) {
                    settings.save(Settings.Key.PostMessagesEnabled, false);
                    item.setTitle(getString(R.string.get_new_message));
                    Intent startNotification = new Intent(this, NotificationService.class);
                    startNotification.setAction(Constants.Action.StopNotification.toString());
                    startService(startNotification);
                } else {
                    settings.save(Settings.Key.PostMessagesEnabled, true);
                    item.setTitle(getString(R.string.stop_posting_messages));
                    Intent startNotification = new Intent(this, NotificationService.class);
                    startNotification.setAction(Constants.Action.StartNotification.toString());
                    startService(startNotification);
                }
                break;
            default:
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSweetSheetShow() {
        Message currentMessage = null;
        try {
            currentMessage = objectMapper.readValue(settings.getString(Settings.Key.CurrentMessage, ""), Message.class);
            if (currentMessage != null) {
                if (currentMessage != null) {
                    sweetSheetHelper.setAuthor(currentMessage.getAuthor());
                    sweetSheetHelper.setMessage(currentMessage.getMessage());
                }
            }
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSweetSheetHide() {

    }

    @Override
    public void onConnectedViaWifi() {
        switch (statemachine.get()) {
            case Init:
            case Loading:
                snackbarHelper.hide();
                EventBus.getDefault().post(new OnConnectedEvent());
        }
    }

    @Override
    public void onDisconnected() {
        switch (statemachine.get()) {
            case Init:
            case Loading:
                snackbarHelper.show();
                EventBus.getDefault().post(new OnDisconnectedEvent());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(OnDismissClickedEvent e) {
        if (optionsMenu != null) {
            MenuItem it = optionsMenu.findItem(R.id.stop_posting);
            if (it != null)
                it.setTitle(getString(R.string.get_new_message));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(NewCurrentMessageEvent e) {
        switch (statemachine.get()) {
            case Ready:

                Message currentMessage = null;
                try {
                    currentMessage = objectMapper.readValue(settings.getString(Settings.Key.CurrentMessage, ""), Message.class);
                    if (currentMessage != null) {
                        showFragment(ImageFragment.newInstance());
                        toolbarHelper.show();
                        actionButtonHelper.show();
                        sweetSheetHelper.setAuthor(currentMessage.getAuthor());
                        sweetSheetHelper.setMessage(currentMessage.getMessage());
                        sweetSheetHelper.show();
                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {

            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
