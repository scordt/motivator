package der.promotivator.ui.util;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


/**
 * Created by simon on 04.02.16.
 */
public class SnackbarHelper {

    private final int resId;
    private final AppCompatActivity activity;
    private Snackbar snackbar;
    private View.OnClickListener listener;

    public SnackbarHelper(AppCompatActivity activity, int resId) {
        this.activity = activity;
        this.resId = resId;
        snackbar = Snackbar.make(activity.findViewById(android.R.id.content),
                activity.getString(resId),
                Snackbar.LENGTH_INDEFINITE);
    }

    public SnackbarHelper setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
        snackbar.setAction(android.R.string.ok, listener);
        return this;
    }

    public SnackbarHelper show() {
        snackbar = Snackbar.make(activity.findViewById(android.R.id.content),
                activity.getString(resId),
                Snackbar.LENGTH_INDEFINITE);
        if(listener != null)
            snackbar.setAction(android.R.string.ok, listener);
        snackbar.show();
        return this;
    }

    public SnackbarHelper hide() {
        snackbar.dismiss();
        return this;
    }
}
