package der.promotivator.ui.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import java.io.IOException;

import der.promotivator.R;
import der.promotivator.model.Message;
import der.promotivator.util.Settings;

import static der.promotivator.PromotivatorApp.settings;
import static der.promotivator.PromotivatorApp.objectMapper;

public class ImageFragment extends Fragment {

    public ImageFragment() {
    }


    public static ImageFragment newInstance() {
        ImageFragment fragment = new ImageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);

        try {
            Message message = objectMapper.readValue(settings.getString(Settings.Key.CurrentMessage, ""), Message.class);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) view.findViewById(R.id.imageView);
            if (message.getImage() != null && message.getImage().length > 0) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(message.getImage(), 0, message.getImage().length, options);
                imageView.setImage(ImageSource.bitmap(bitmap));
            } else {
                imageView.setImage(ImageSource.resource(R.mipmap.no_image));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return view;
    }

}
