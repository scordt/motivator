package der.promotivator.ui.util;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * Created by simon on 04.02.16.
 */
public class TextViewHelper {

    private TextView tv;

    public TextViewHelper(AppCompatActivity activity, int resId) {
        tv = (TextView) activity.findViewById(resId);
    }

    public TextViewHelper show() {
        tv.setVisibility(View.VISIBLE);
        return this;
    }

    public TextViewHelper hide() {
        tv.setVisibility(View.INVISIBLE);
        return this;
    }

    public void setText(String s) {
        tv.setText(s);
    }
}
