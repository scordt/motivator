package der.promotivator.ui.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by simon on 05.02.16.
 */
public class ImageViewHelper {

    private final AppCompatActivity activity;
    private ImageView imageView;
    private byte[] imageBytes = null;
    private Bitmap bmp;

    public ImageViewHelper(AppCompatActivity activity, int resId) {
        imageView = (ImageView) activity.findViewById(resId);
        this.activity = activity;
    }

    public ImageViewHelper setOnClickListener(View.OnClickListener listener) {
        imageView.setOnClickListener(listener);
        return this;
    }

    public ImageViewHelper setImage(byte[] imageBytes) {
        if (this.imageBytes != imageBytes) {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            this.bmp = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length, options);
            imageView.setImageBitmap(bmp);
            //imageView.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);
        }
        return this;
    }

    public ImageViewHelper setImage(int resId) {
        imageView.setImageResource(resId);
        return this;
    }


    public ImageViewHelper show() {
        if (!imageView.isShown())
            imageView.setVisibility(View.VISIBLE);
        return this;
    }

    public ImageViewHelper hide() {
        if (imageView.isShown())
            imageView.setVisibility(View.INVISIBLE);
        return this;
    }

    public ImageViewHelper clearImage() {
        imageView.setImageResource(android.R.color.transparent);
        return this;
    }

    /*public ImageViewHelper fit() {
        int screenWidth = activity.getWindow().getWindowManager().getDefaultDisplay().getWidth();
        int screenHeight = activity.getWindow().getWindowManager().getDefaultDisplay().getHeight();

        int width = bmp.getWidth();
        int height = bmp.getHeight();

        float scale = 0;
        if(height>width){
            scale = (float) width / (float) height;
        } else if(width>height){
            scale = (float) height / (float) width;
        }

        float newWidth = (float) screenWidth * scale;
        float newHeight = (float) screenHeight * scale;
        if(newWidth > 0 && newHeight > 0) {
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, (int) newWidth, (int) newHeight, true);
            imageView.setImageBitmap(scaledBitmap);
        }

        /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenHeight, (int)newWidth);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        imageView.setLayoutParams(params);

        return this;
    }*/
}
