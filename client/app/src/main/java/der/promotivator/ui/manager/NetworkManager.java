package der.promotivator.ui.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Handler;

import static der.promotivator.PromotivatorApp.network;

/**
 * Created by simon on 04.02.16.
 */
public class NetworkManager {

    private static final int UNDEFINED = 2;
    private static final int TRUE = 1;
    private static final int FALSE = 0;

    private int wasPreviouslyAvail = UNDEFINED;

    private final NetworkListener   networkListener;
    private final WifiReceiver      receiver;
    private final Context           context;

    public NetworkManager(Context context) {

        this.context = context;
        networkListener = (NetworkListener) context;
        receiver = new WifiReceiver();

        registerReceiver();
        triggerCallbacks();
    }

    public void registerReceiver() {
        if (context != null) {
                final IntentFilter filters = new IntentFilter();
                filters.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
                filters.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
                context.registerReceiver(receiver, filters);

        }
    }

    public void unregisterReceiver() {
        if (receiver != null && context != null)
            context.unregisterReceiver(receiver);
    }

    public void triggerCallbacks() {

        if (networkListener == null)
            return;

        // ugly shit: give system some time to realize it has wifi... can't do much about it.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int currentlyAvail = network.isConnectedToWiFiInt();
                if (currentlyAvail != wasPreviouslyAvail) {
                    wasPreviouslyAvail = currentlyAvail;
                    if (wasPreviouslyAvail == 1) {
                        networkListener.onConnectedViaWifi();
                    } else if(wasPreviouslyAvail == 0) {
                        networkListener.onDisconnected();
                    }
                }
            }
        }, 2000L);
    }

    private class WifiReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            triggerCallbacks();
        }
    }
}
