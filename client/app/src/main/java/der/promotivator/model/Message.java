package der.promotivator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;

import static der.promotivator.PromotivatorApp.objectMapper;

/**
 * Created by simon on 05.02.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@DatabaseTable
public class Message {

    public static final String ID = "id";
    public static final String AUTHOR = "author";
    public static final String MESSAGE = "message";
    public static final String IMAGE = "image";
    public static final String ALREADY_USED = "already_used";

    @DatabaseField(columnName = ID, generatedId = true)
    private Long id;

    @DatabaseField(columnName = AUTHOR)
    private String author;

    @DatabaseField(columnName = MESSAGE)
    private String message;

    @DatabaseField(dataType = DataType.BYTE_ARRAY, columnName = IMAGE)
    private byte[] image;

    @DatabaseField(columnName = ALREADY_USED)
    private boolean alreadyUsed = false;

    public Message() {
    }

    public Message(String author, String message) {
        this.author = author;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public boolean isAlreadyUsed() {
        return alreadyUsed;
    }

    public void setAlreadyUsed(boolean alreadyUsed) {
        this.alreadyUsed = alreadyUsed;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Message message1 = (Message) o;

        return new org.apache.commons.lang3.builder.EqualsBuilder()
                .append(author, message1.author)
                .append(message, message1.message)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new org.apache.commons.lang3.builder.HashCodeBuilder(17, 37)
                .append(author)
                .append(message)
                .toHashCode();
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public static Message fromJson(String json) throws IOException {
        return objectMapper.readValue(json, Message.class);
    }
}

