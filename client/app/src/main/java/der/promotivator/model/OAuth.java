package der.promotivator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;

/**
 * Created by simon on 08.02.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OAuth {

    private String access_token;
    private String token_type;
    private String refresh_token;
    private String scope;
    private int expires_in;
    private long expires_at;
    private String client_id;
    private String client_secret;

    public OAuth() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        OAuth oAuth = (OAuth) o;

        return new EqualsBuilder()
                .append(expires_in, oAuth.expires_in)
                .append(access_token, oAuth.access_token)
                .append(token_type, oAuth.token_type)
                .append(refresh_token, oAuth.refresh_token)
                .append(scope, oAuth.scope)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(access_token)
                .append(token_type)
                .append(refresh_token)
                .append(scope)
                .append(expires_in)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "OAuth{" +
                "access_token='" + access_token + '\'' +
                ", token_type='" + token_type + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", scope='" + scope + '\'' +
                ", expires_in=" + expires_in +
                '}';
    }

    public static OAuth fromJson(String json) throws IOException {
        return objectMapper.readValue(json, OAuth.class);
    }

    public String toJson() throws JsonProcessingException {
        return objectMapper.writeValueAsString(this);
    }

    public long getExpires_at() {
        return expires_at;
    }

    public void setExpires_at(long expires_at) {
        this.expires_at = expires_at;
    }

    public Map<String, String> getRefreshBodyAsMap() {
        Map<String, String> refreshBody = new HashMap<>();
        refreshBody.put("refresh_token", refresh_token);
        refreshBody.put("grant_type", "refresh_token");
        refreshBody.put("client_id", client_id);
        refreshBody.put("client_secret", client_secret);
        return null;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}
