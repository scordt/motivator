package der.promotivator.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import der.promotivator.model.OAuth;

import static der.promotivator.PromotivatorApp.objectMapper;
import static der.promotivator.PromotivatorApp.settings;

/**
 * Created by simon on 07.02.16.
 */
public class Http {

    private static final String REQUEST_METHOD_GET = "GET";


    public interface OnDataInputStreamListener {
        void onReadLine(String line);
    }

    public static final String REQUEST_METHOD_POST = "POST";

    public String get(String url) throws IOException, NotAuthorizedException, ClientErrorException {
        HttpURLConnection urlConnection = null;

        try {

            OAuth auth = objectMapper.readValue(settings.getString(Settings.Key.OAuth, ""), OAuth.class);

            URL u = new URL(url);
            urlConnection = (HttpURLConnection) u.openConnection();
            urlConnection.setRequestMethod(REQUEST_METHOD_GET);
            urlConnection.setRequestProperty("Authorization", "Bearer " + auth.getAccess_token());
            urlConnection.setReadTimeout(8000);
            urlConnection.setConnectTimeout(12000);

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 401) {
                throw new NotAuthorizedException();
            } else if (responseCode >= 400) {
                throw new ClientErrorException(responseCode);
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return readStreamToString(in);
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }
    }

    public InputStream getInputStream(String url) throws IOException, NotAuthorizedException, ClientErrorException {
        HttpURLConnection urlConnection = null;

        try {

            OAuth auth = objectMapper.readValue(settings.getString(Settings.Key.OAuth, ""), OAuth.class);

            URL u = new URL(url);
            urlConnection = (HttpURLConnection) u.openConnection();
            urlConnection.setRequestMethod(REQUEST_METHOD_GET);
            urlConnection.setRequestProperty("Authorization", "Bearer " + auth.getAccess_token());
            urlConnection.setReadTimeout(8000);
            urlConnection.setConnectTimeout(12000);

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 401) {
                throw new NotAuthorizedException();
            } else if (responseCode >= 400) {
                throw new ClientErrorException(responseCode);
            }

            return new BufferedInputStream(urlConnection.getInputStream());

        } finally {
            /*if(urlConnection != null)
                urlConnection.disconnect();*/
        }
    }

    public void getAttachStreamListener(String url, OnDataInputStreamListener listener) throws IOException, NotAuthorizedException, ClientErrorException {
        HttpURLConnection urlConnection = null;

        try {

            OAuth auth = objectMapper.readValue(settings.getString(Settings.Key.OAuth, ""), OAuth.class);

            URL u = new URL(url);
            urlConnection = (HttpURLConnection) u.openConnection();
            urlConnection.setRequestMethod(REQUEST_METHOD_GET);
            urlConnection.setRequestProperty("Authorization", "Bearer " + auth.getAccess_token());
            urlConnection.setReadTimeout(8000);
            urlConnection.setConnectTimeout(12000);

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 401) {
                throw new NotAuthorizedException();
            } else if (responseCode >= 400) {
                throw new ClientErrorException(responseCode);
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader rd = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = rd.readLine()) != null) {
                listener.onReadLine(line);
            }
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }
    }

    public String postFormData(String url, Map<String, String> data) throws IOException, ClientErrorException, NotAuthorizedException {
        return postFormData(url, null, data);
    }

    public String postFormData(String url, Map<String, String> requestProperties, Map<String, String> data) throws IOException, NotAuthorizedException, ClientErrorException {

        HttpURLConnection urlConnection = null;

        try {

            URL u = new URL(url);
            urlConnection = createConnection(u, REQUEST_METHOD_POST);

            if (requestProperties != null) {
                for (Map.Entry<String, String> entry : requestProperties.entrySet()) {
                    urlConnection.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            writeStream(out, generateFormData(data));

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 401) {
                throw new NotAuthorizedException();
            } else if (responseCode >= 400) {
                throw new ClientErrorException(responseCode);
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return readStreamToString(in);

        } finally {
            if(urlConnection != null)
            urlConnection.disconnect();
        }
    }

    private HttpURLConnection createConnection(URL url, String requestMethod) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setReadTimeout(8000);
        urlConnection.setConnectTimeout(12000);
        urlConnection.setRequestMethod(requestMethod);
        urlConnection.setChunkedStreamingMode(0);
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);
        return urlConnection;
    }

    private String readStreamToString(InputStream in) throws IOException {

        StringBuilder sb = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(in));
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    private void writeStream(OutputStream out, String data) throws IOException {

        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(out, "UTF-8"));
        writer.write(data);
        writer.flush();
        writer.close();
        out.close();
    }

    private String generateFormData(Map<String, String> data) {

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : data.entrySet())
            sb.append("&")
                    .append(entry.getKey())
                    .append("=")
                    .append(entry.getValue());


        sb.deleteCharAt(0);
        return sb.toString();
    }

    public class NotAuthorizedException extends Throwable {
    }

    public class ClientErrorException extends Throwable {

        private int statusCode;

        public ClientErrorException(int statusCode) {
            this.statusCode = statusCode;
        }

        public int getStatusCode() {
            return statusCode;
        }

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }
    }
}
