package der.promotivator.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by simon on 04.02.16.
 */
public class Network {

    private final Context context;

    public Network(Context context) {
        this.context = context;
    }

    public boolean isConnectedToWiFi() {

        if(context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(cm == null)
            return false;

        NetworkInfo mWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi != null
                && mWifi != null
                && mWifi.isConnectedOrConnecting();
        //return activeNetwork.isConnectedOrConnecting();
    }

    public int isConnectedToWiFiInt() {

        if(context == null) {
            return 0;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(cm == null)
            return 0;

        NetworkInfo mWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return mWifi != null
                && mWifi != null
                && mWifi.isConnectedOrConnecting() ? 1 : 0;
        //return activeNetwork.isConnectedOrConnecting();
    }
}
