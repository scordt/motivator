package der.promotivator.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by simon on 04.02.16.
 */
public class Settings {

    public enum Key {

        PostMessagesEnabled("postMessages"),
        ExpiresIn("expiresIn"),
        AccessToken("accessToken"),
        RefreshToken("refreshToken"),
        Scope("scope"),
        TokenType("tokenType"),
        LastUpdate("lastUpdate"),
        Hash("hash"),
        CurrentMessage("currentMessage"),
        FirstRun("firstrun"),
        NumMessages("numMessages"),
        OAuth("oauth");

        private final String identifier;

        Key(String identifier) {
            this.identifier = identifier;
        }

        public String getIdentifier() {
            return identifier;
        }
    }

    private final SharedPreferences sharedPreferences;

    public Settings(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    // string
    public void save(Key key, String value) {
        SharedPreferences.Editor editor = getPrefsEditor();
        editor.putString(key.getIdentifier(), value);
        editor.commit();
    }

    // int
    public void save(Key prefsKey, int value) {
        SharedPreferences.Editor editor = getPrefsEditor();
        editor.putInt(prefsKey.getIdentifier(), value);
        editor.commit();
    }

    // boolean
    public void save(Key key, boolean value) {
        SharedPreferences.Editor editor = getPrefsEditor();
        editor.putBoolean(key.getIdentifier(), value);
        editor.commit();
    }

    // long
    public void save(Key key, long value) {
        SharedPreferences.Editor editor = getPrefsEditor();
        editor.putLong(key.getIdentifier(), value);
        editor.commit();
    }

    public Integer getInt(Key key, int defaultValue) {
        return sharedPreferences.getInt(key.getIdentifier(), defaultValue);
    }

    public String getString(Key key, String defaultValue) {
        return sharedPreferences.getString(key.getIdentifier(), defaultValue);
    }

    public boolean getBoolean(Key key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key.getIdentifier(), defaultValue);
    }

    public long getLong(Key key, long defaultValue) {
        return sharedPreferences.getLong(key.getIdentifier(), defaultValue);
    }

    private SharedPreferences.Editor getPrefsEditor() {
        return sharedPreferences.edit();
    }


}
