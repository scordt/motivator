package der.promotivator.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import der.promotivator.R;
import der.promotivator.model.Message;

/**
 * Created by simon on 05.02.16.
 */
public class Database extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "mischas_bday.db";
    private static final int DATABASE_VERSION = 6;

    private Dao<Message, Long> messageDao;

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Message.class);
        } catch (SQLException e) {
            Log.e(Database.class.getName(), "Unable to create datbases", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
        try {
            TableUtils.dropTable(connectionSource, Message.class, true);
            onCreate(sqliteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e(Database.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
                    + newVer, e);
        }
    }


    public Dao<Message, Long> getMessageDao() throws SQLException {
        if (messageDao == null) {
            messageDao = getDao(Message.class);
        }
        return messageDao;
    }

    public void truncate(Class<?> cls) {
        try {
            TableUtils.clearTable(connectionSource, cls);
        } catch (SQLException e) {
            Log.e(Database.class.getName(), "Unable to create datbases", e);
        }
    }
}
