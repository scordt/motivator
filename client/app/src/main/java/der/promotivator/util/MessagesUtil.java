package der.promotivator.util;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import der.promotivator.model.Message;
import static der.promotivator.PromotivatorApp.database;

/**
 * Created by simon on 08.02.16.
 */
public class MessagesUtil {

    public static Message nextRandom() throws SQLException, JsonProcessingException {

        List<Message> unusedMessages = database.getMessageDao().queryForEq(Message.ALREADY_USED, false);
        Message randomMessage = null;
        if(unusedMessages.size() == 0) {
            List<Message> allMessages = database.getMessageDao().queryForAll();
            for(Message m : allMessages) {
                m.setAlreadyUsed(false);
                database.getMessageDao().update(m);
            }
            unusedMessages = allMessages;
        }
        if(unusedMessages.size() > 0) {
            randomMessage = unusedMessages.get(new Random().nextInt(unusedMessages.size()));

            if (randomMessage != null) {
                randomMessage.setAlreadyUsed(true);
                database.getMessageDao().update(randomMessage);
                return randomMessage;
            }
        }
        return null;
    }
}
