package der.promotivator.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by simon on 01.02.16.
 */
public class JsonUtil {

    public static Map<String, String> jsonToMap(String json) throws IOException {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);
        TypeReference<HashMap<String,String>> typeRef
                = new TypeReference<HashMap<String,String>>() {};

        HashMap<String,String> o = mapper.readValue(new ByteArrayInputStream(json.getBytes("UTF-8")) , typeRef);
        System.out.println("Got " + o);
        return o;
    }

}
