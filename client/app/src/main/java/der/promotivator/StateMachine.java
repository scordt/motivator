package der.promotivator;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Simon on 05.02.16.
 */
public class StateMachine {

    public static final String KEY = "appstate";
    public enum AppState {
        Init, Loading, Ready;
    }

    private final SharedPreferences             sharedPreferences;
    private final Set<OnStateChangeListener>    listeners;

    public StateMachine(Application context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        listeners = new HashSet<OnStateChangeListener>();
    }

    /**
     * Register listener
     * @param listener
     */
    public void register(OnStateChangeListener listener) {
        listeners.add(listener);
    }

    /**
     * Unregister listener
     * @param listener
     */
    public void unregister(OnStateChangeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Persists AppState to SharedPreferences
     */
    public void set(AppState state) {

        AppState prevState = get();
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(KEY, state.ordinal());
        editor.commit();

        for(OnStateChangeListener listener : listeners)
            listener.onStateChange(prevState, state);

    }

    /**
     *
     * @return the current AppState or Init, if none persited before
     */
    public AppState get() {
        AppState val = AppState.values()[sharedPreferences.getInt(KEY, AppState.Init.ordinal())];
        return val;
    }


    public interface OnStateChangeListener {

        /**
         * @param prevState
         * @param nextState
         */
        void onStateChange(AppState prevState, AppState nextState);

    }
}
