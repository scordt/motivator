package der.promotivator;

import android.app.Application;
import android.content.Intent;

import com.fasterxml.jackson.databind.ObjectMapper;

import der.promotivator.receiver.AlarmReceiver;
import der.promotivator.service.DownloadService;
import der.promotivator.service.NotificationService;
import der.promotivator.util.Database;
import der.promotivator.util.Http;
import der.promotivator.util.Network;
import der.promotivator.util.Settings;


/**
 * Created by simon on 05.02.16.
 */
public class PromotivatorApp extends Application {

    public static Settings settings;
    public static Network network;
    public static ObjectMapper objectMapper;
    public static Database database;
    public static StateMachine statemachine;
    public static Http http;

    @Override
    public void onCreate() {

        super.onCreate();

        settings = new Settings(this);
        network = new Network(this);
        database = new Database(this);
        statemachine = new StateMachine(this);
        http = new Http();
        objectMapper = new ObjectMapper();

        if(settings.getBoolean(Settings.Key.PostMessagesEnabled, true)) {
            Intent startNotification = new Intent(this, NotificationService.class);
            startNotification.setAction(Constants.Action.StartNotification.toString());
            startService(startNotification);
        }

        AlarmReceiver.schedule(this);

        Intent startDownload = new Intent(this, DownloadService.class);
        startService(startDownload);

    }

}
