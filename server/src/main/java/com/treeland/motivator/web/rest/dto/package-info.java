/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.treeland.motivator.web.rest.dto;
