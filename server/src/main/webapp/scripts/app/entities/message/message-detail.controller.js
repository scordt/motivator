'use strict';

angular.module('motivatorserverApp')
    .controller('MessageDetailController', function ($scope, $rootScope, $stateParams, DataUtils, entity, Message) {
        $scope.message = entity;
        $scope.load = function (id) {
            Message.get({id: id}, function(result) {
                $scope.message = result;
            });
        };
        var unsubscribe = $rootScope.$on('motivatorserverApp:messageUpdate', function(event, result) {
            $scope.message = result;
        });
        $scope.$on('$destroy', unsubscribe);

        $scope.byteSize = DataUtils.byteSize;
    });
