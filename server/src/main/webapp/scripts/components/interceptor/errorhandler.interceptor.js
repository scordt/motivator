'use strict';

angular.module('motivatorserverApp')
    .factory('errorHandlerInterceptor', function ($q, $rootScope) {
        return {
            'responseError': function (response) {
                if (!(response.status == 401 && response.data.path.indexOf("/api/account") == 0 )){
	                $rootScope.$emit('motivatorserverApp.httpError', response);
	            }
                return $q.reject(response);
            }
        };
    });