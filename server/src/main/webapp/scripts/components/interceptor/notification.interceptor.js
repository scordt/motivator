 'use strict';

angular.module('motivatorserverApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-motivatorserverApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-motivatorserverApp-params')});
                }
                return response;
            }
        };
    });
